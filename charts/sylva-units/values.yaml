# Default values for sylva-units.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

# generic helm chart release name overrides
nameOverride: ""
fullnameOverride: ""

# registry secrets
registry_secret: {}
  # when using gitlab, git creds can be used as registry creds
  #registry.gitlab.com:
  #  username: your_user_name
  #  password: glpat-XXXXX

git_repo_spec_default:
  interval: 60m0s
  gitImplementation: libgit2

git_repo_templates: # template to generate Flux GitRepository resources
  # <repo-name>:
  #   spec:  # partial spec for a Flux GitRepository
  #     url: https://gitlab.com/sylva-projects/sylva-core.git
  #     #secretRef: # is autogenerated based on 'auth'
  #     ref: # can be overridden per-unit, with 'ref_override'
  #       branch: main
  #   auth: # optional 'username'/'password' dict containing git authentication information
  #   existing_gitrepository: optional, when this value is set the specified GitRepository will be used instead of creating one based on 'spec'

  sylva-core:
    spec:
      url: https://gitlab.com/sylva-projects/sylva-core.git
      ref:
        # tag/commit will take precedence, see https://fluxcd.io/flux/units/source/api/#source.toolkit.fluxcd.io/v1beta2.GitRepositoryRef
        branch: main

  capi-rancher-import:
    spec:
      url: https://gitlab.com/sylva-projects/sylva-elements/helm-charts/capi-rancher-import.git
      ref:
        tag: 0.1.0

  weave-gitops:
    spec:
      url: https://github.com/weaveworks/weave-gitops.git
      ref:
        tag: v0.10.2

  metal3:
    spec:
      url: https://gitlab.com/sylva-projects/sylva-elements/helm-charts/metal3.git
      ref:
        tag: 0.1.0

  cluster-vsphere:
    spec:
      url: https://gitlab.com/sylva-projects/sylva-elements/helm-charts/cluster-vsphere.git
      ref:
        tag: 0.1.0

  local-path-provisioner:
    spec:
      url: https://github.com/rancher/local-path-provisioner.git
      ref:
        tag: v0.0.23

helm_repo_spec_default:
  interval: 60m0s


# this defines the default .spec for a Kustomization resource
# generated for each item of 'units'
unit_kustomization_spec_default: # default .spec for a Kustomization
  force: false
  prune: true
  interval: 15m
  retryInterval: 1m
  wait: false
  timeout: 30s

# this defines the default .spec for a HelmRelease resource
# generated a unit with a "helmrelease_spec" field
unit_helmrelease_spec_default:  # default for the .spec of a HelmRelease
  interval: 15m
  install:
    remediation:
      retries: 10
      remediateLastFailure: true

# this defines the default .spec for a Kustomization resource containing the HelmRelease resource
# generated a unit with a "helmrelease_spec" field
unit_helmrelease_kustomization_spec_default:
  path: ./kustomize-units/helmrelease-generic
  sourceRef:
    kind: GitRepository
    # GitRepository created by templates/gitrepository-self.yaml unless an existing repository is used
    name: '{{ hasKey (index .Values.git_repo_templates "sylva-core") "existing_gitrepository" | ternary (index .Values.git_repo_templates "sylva-core").existing_gitrepository "sylva-core" }}'

  wait: true

# this base template is used to generate the special "sylva-units-status" Kustomization
# which is a special Kustomization that depends on all other Kustomizations
# produced by the chart. Looking at its status allows to tell if all units have been deployed.
#
# (see templates/status-kustomization.yaml for the code injecting the dependsOn)
#
status_unit_kustomization_spec_default:
  path: ./kustomize-units/sylva-units-status
  sourceRef:
    kind: GitRepository
    # GitRepository created by templates/gitrepository-self.yaml unless an existing repository is used
    name: '{{ hasKey (index .Values.git_repo_templates "sylva-core") "existing_gitrepository" | ternary (index .Values.git_repo_templates "sylva-core").existing_gitrepository "sylva-core" }}'
  wait: true
  patches:
    - target:
        kind: ConfigMap
      patch: |
        kind: ConfigMap
        metadata:
          name: _ignored_
        data:
          sylva-core-ref: "{{ (index .Values.git_repo_templates "sylva-core").spec.ref | toYaml }}"
          sylva-units-release-revision: "{{ .Release.Revision }}"


# this defines Flux HelmRelease objects, and for each  # FIXME
# a corresponding GitRepository (and Secret, TODO: the Secret don't need to be generated for each unit)
units:
  # <unit-name>:
  #   enabled: boolean
  #   repo: <name of the repo under 'git_repo_templates'> (for use with kustomization_spec)
  #   helm_repo_url: URL of the Helm repo to use (for use with helmrelease_spec, but not mandatory, 'repo' can be used as well to use a git repo)
  #   labels: (optional) dict holding labels to add to the resources for this unit
  #   ref_override: optional, if defined, this dict will be used for the GitRepository overriding spec.ref (not used if some helm_repo_* is set)
  #   depends_on: list of '{name: unit}' maps defining the dependencies of this unit (this is injected in the unit Kustomization as dependsOn field)
  #   helmrelease_spec:  # optionnal, contains a partial spec for a FluxCD HelmRelease, all the
  #                      # key things are generated from unit_helmrelease_spec_default
  #                      # and from other fields in the unit definition
  #   kustomization_spec:  # contains a partial spec for a FluxCD Kustomization, most of the
  #                        # things are generated from unit_kustomization_spec_default
  #     # sourceRef is generated from .git_repo field
  #     path: ./path-to-unit-under-repo
  #     # the final path will hence be:
  #     # - <git repo template>.spec.url + <unit>.spec.path  (if <git repo template> has spec.url defined)
  #
  #   helm_secret_values: # (dict), if set what is put here is injected in HelmRelease.valuesFrom as a Secret
  #   kustomization_substitute_secrets: # (dict), if set what is put here is injected in Kustomization.postBuild.substituteFrom as a Secret

  flux-system:
    # note that Flux is always installed on the current cluster as a pre-requisite to installing the chart
    # this units contains Flux definitions *to manage the Flux system itself via gitops*
    enabled: yes
    repo: sylva-core
    kustomization_spec:
      path: ./kustomize-units/flux-system/base
      targetNamespace: flux-system
      wait: true
      postBuild:
        substitute:
          var_substitution_enabled: "true" # To force substitution when configmap does not exist
        substituteFrom:
        - kind: ConfigMap
          name: proxy-env-vars
          optional: true

  kubed:
    enabled: no
    # Kubed may be used to copy secrets from a namespace to another. It can be usefull for example, if some unit need a registry secrets to pull images.
    # This chart could be used to generate registry secret, but it would have to create the unit namespace too in that case, which is not so clean
    # Using kubed, you may just add a label "add-registry-secret: sylva" to the target namespace, and kubed will copy secret in it
    helm_repo_url: https://charts.appscode.com/stable
    helmrelease_spec:
      chart:
        spec:
          chart: kubed
          version: v0.13.2
      targetNamespace: kubed
      install:
        createNamespace: true
      values:
        installCRDs: true

  cert-manager:
    enabled: yes
    helm_repo_url: https://charts.jetstack.io
    helmrelease_spec:
      chart:
        spec:
          chart: cert-manager
          version: v1.8.2
      targetNamespace: cert-manager
      install:
        createNamespace: true
      values:
        installCRDs: true

  trivy-operator:
    enabled: yes
    helm_repo_url: https://aquasecurity.github.io/helm-charts/
    helmrelease_spec:
      chart:
        spec:
          chart: trivy-operator
          version: v0.10.2
      targetNamespace: trivy-system
      install:
        createNamespace: true
      values:
        trivy:
          httpProxy: '{{ .Values.proxies.https_proxy }}'
          httpsProxy: '{{ .Values.proxies.https_proxy }}'
          noProxy: '{{ .Values.proxies.no_proxy }}'
          severity: UNKNOWN,HIGH,CRITICAL

# sylva-ca provides a Certificate Authority for Components of the management cluster
  sylva-ca:
    enabled: yes
    depends_on:
    - name: cert-manager
    repo: sylva-core
    kustomization_spec:
      path: ./kustomize-units/sylva-ca
      targetNamespace: cert-manager
      wait: true

#  vault-tls issues certificates, meant to be used by Vault, from sylva-ca
  vault-tls:
    enabled: yes
    depends_on:
    - name: sylva-ca
    repo: sylva-core
    kustomization_spec:
      path: ./kustomize-units/vault-tls
      targetNamespace: vault
      wait: true
      postBuild:
        substitute:
          VAULT_DNS: '{{ .Values.cluster.vault.external_hostname }}'

# Vault assumes that the certificates vault-tls have been issued
  vault:
    enabled: yes
    depends_on:
    - name: vault-tls
    - '{{ tuple (dict "name" "ingress-nginx") (.Values.cluster.capi_providers.bootstrap_provider | eq "cabpk") | include "set-only-if" }}'
    helm_repo_url: https://helm.releases.hashicorp.com
    helmrelease_spec:
      chart:
        spec:
          chart: vault
          version: v0.22.0
      targetNamespace: vault
      values:
        server:
          extraEnvironmentVars:
            VAULT_CACERT: /vault/userconfig/vault-tls/ca.crt
            VAULT_ADDR: https://127.0.0.1:8200
          extraVolumes:
          - type: secret
            name: vault-tls
          ha:
            enabled: true
            replicas: 1
            raft:
              enabled: true
              setNodeId: true
              # FIXME: update storage to raft when local storage on the management cluster is available
              config: |
                 ui = true
                 listener "tcp" {
                   tls_disable = 0
                   address = "[::]:8200"
                   cluster_address = "[::]:8201"
                   tls_cert_file = "/vault/userconfig/vault-tls/tls.crt"
                   tls_key_file = "/vault/userconfig/vault-tls/tls.key"
                 }
                 storage "inmem" {}
          dataStorage:
            enabled: true
            size: 10Gi
            mountPath: "/vault/data"
            storageClass: local-path
          readinessProbe:
            enabled: false
          ingress:
            enabled: true
            annotations:
              kubernetes.io/ingress.class: nginx
              nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
              #cert-manager.io/issuer: "ca-issuer"
            pathType: Prefix
            activeService: false
            hosts:
            - host: '{{ .Values.cluster.vault.external_hostname }}'
              paths: []
            extraPaths:
            - path: /*
              pathType: Prefix
              backend:
                service:
                  name: vault-ui
                  port:
                    number: 8200
            tls:
            - secretName: vault-tls
              hosts:
              - '{{ .Values.cluster.vault.external_hostname }}'
        ui:
          enabled: true
        csi:
          enabled: true
          volumes:
          - name: vault-ca
            secret:
              secretName: vault-tls
          volumeMounts:
          - name: vault-ca
            mountPath: /vault/tls
            readOnly: true

  cis-operator-crd:
    enabled: '{{ .Values.cluster.capi_providers.bootstrap_provider | eq "cabpr" | include "preserve-type" }}'
    helm_repo_url: https://charts.rancher.io
    helmrelease_spec:
      chart:
        spec:
          chart: rancher-cis-benchmark-crd
          version: v3.0.0
      targetNamespace: cis-operator-system
      install:
        createNamespace: true

  cis-operator:
    enabled: '{{ .Values.cluster.capi_providers.bootstrap_provider | eq "cabpr" | include "preserve-type" }}'
    depends_on:
      - name: cis-operator-crd
    helm_repo_url: https://charts.rancher.io
    helmrelease_spec:
      chart:
        spec:
          chart: rancher-cis-benchmark
          version: v3.0.0
      targetNamespace: cis-operator-system

  # this allows for running a CIS scan for management cluster
  # generates a report which can be viewed and downloaded in CSV from the Rancher UI, at https://rancher.sylva/dashboard/c/local/cis/cis.cattle.io.clusterscan
  cis-operator-scan:
    enabled: '{{ .Values.cluster.capi_providers.bootstrap_provider | eq "cabpr" | include "preserve-type" }}'
    depends_on:
      - name: cis-operator
    repo: sylva-core
    kustomization_spec:
      path: ./kustomize-units/cis-operator-scan
      wait: true
      postBuild:
        substitute:
          SCAN_PROFILE: '{{ .Values.cluster.cis_benchmark_scan_profile }}'

  keycloak:
    enabled: yes
    depends_on:
    - name: sylva-ca
    - '{{ tuple (dict "name" "ingress-nginx") (.Values.cluster.capi_providers.bootstrap_provider | eq "cabpk") | include "set-only-if" }}'
    repo: sylva-core
    kustomization_spec:
      path: ./kustomize-units/keycloak
      targetNamespace: keycloak
      postBuild:
        substitute:
          KEYCLOAK_DNS: '{{ .Values.cluster.keycloak.external_hostname }}'
      wait: false

  capi:
    enabled: yes
    repo: sylva-core
    depends_on:
      - name: cert-manager
    kustomization_spec:
      path: ./kustomize-units/capi
      wait: true

  capd:
    enabled: '{{ .Values.cluster.capi_providers.infra_provider | eq "capd" | include "preserve-type" }}'
    repo: sylva-core
    depends_on:
      - name: cert-manager
    kustomization_spec:
      path: ./kustomize-units/capd
      postBuild:
        substitute:
          DOCKER_HOST: '{{ .Values.cluster.capd.docker_host }}'
      wait: true

  capo:
    enabled: '{{ .Values.cluster.capi_providers.infra_provider | eq "capo" | include "preserve-type" }}'
    repo: sylva-core
    depends_on:
      - name: cert-manager
    kustomization_spec:
      path: ./kustomize-units/capo
      wait: true

  capm3:
    enabled: '{{ .Values.cluster.capi_providers.infra_provider | eq "capm3" | include "preserve-type" }}'
    repo: sylva-core
    depends_on:
      - name: cert-manager
    kustomization_spec:
      path: ./kustomize-units/capm3
      wait: true

  capv:
    enabled: '{{ .Values.cluster.capi_providers.infra_provider | eq "capv" | include "preserve-type" }}'
    repo: sylva-core
    depends_on:
      - name: cert-manager
    kustomization_spec:
      path: ./kustomize-units/capv
      wait: true

  cabpk:  # kubeadm
    enabled: '{{ .Values.cluster.capi_providers.bootstrap_provider | eq "cabpk" | include "preserve-type" }}'
    repo: sylva-core
    depends_on:
      - name: cert-manager
    kustomization_spec:
      path: ./kustomize-units/cabpk
      wait: true

  cabpr:  # RKE2
    enabled: '{{ .Values.cluster.capi_providers.bootstrap_provider | eq "cabpr" | include "preserve-type" }}'
    repo: sylva-core
    depends_on:
      - name: cert-manager
    kustomization_spec:
      path: ./kustomize-units/cabpr
      wait: true

  metal3:
    enabled: no
    depends_on:
    - name: cert-manager
    repo: metal3
    helmrelease_spec:
      chart:
        spec:
          chart: egate/metal3
      install:
        createNamespace: true
      targetNamespace: metal3-system
      values:  # see https://gitlab.com/sylva-projects/sylva-elements/helm-charts/metal3/-/blob/main/values.yaml
        global:
          storageClass: local-path
        # ironicIPADownloaderBaseURI:
        httpProxy: '{{ .Values.proxies.https_proxy }}'
        httpsProxy: '{{ .Values.proxies.https_proxy }}'
        noProxy: '{{ .Values.proxies.no_proxy }}'
        persistence:
          ironic:
            accessMode: "ReadWriteOnce"
        services:
          ironic:
          # Specify the IP address used by Ironic service
            ironicIP: '{{ .Values.cluster.metal3.ironic_ip }}'
        mariadb:
          auth:
            # if rootPassword is left empty a random one is generated at each installation
            # setting one is suggested in order for mariadb to be able to reuse an already initialized pvc
            rootPassword: "changeme"
          persistence:
            storageClass: local-path

  local-path-provisioner:
    enabled: yes
    repo: local-path-provisioner
    helmrelease_spec:
      chart:
        spec:
          chart: deploy/chart/local-path-provisioner
      targetNamespace: local-path-storage
      install:
        createNamespace: true

  cluster:
    enabled: yes
    repo: sylva-core
    depends_on:
      - name: capi
      - name: '{{ .Values.cluster.capi_providers.infra_provider }}'
      - name: '{{ .Values.cluster.capi_providers.bootstrap_provider }}'
    labels:
      suspend-on-pivot: "yes"  # this unit must be suspended before pivot
    kustomization_spec:
      # see note below under .cluster
      # the choice made here, for now, requires setting other values consistently under .cluster.xxx
      path: ./kustomize-units/cluster-manifests/kubeadm-capd/base
      postBuild:
        substitute:
          REGISTRY_MIRROR: '{{ .Values.dockerio_registry_mirror }}'
        substituteFrom:
        - kind: ConfigMap
          name: cluster-vars
        - kind: ConfigMap
          name: proxy-env-vars
      healthChecks:
        - apiVersion: cluster.x-k8s.io/v1beta1
          kind: Cluster
          name: management-cluster
          namespace: default
      components:
        - '{{ tuple "../../components/proxies/KubeadmControlPlane"
                    (and .Values.proxies.http_proxy (eq .Values.cluster.capi_providers.bootstrap_provider "cabpk")) | include "set-only-if" }}'
        - '{{ tuple "../../components/proxies/RKE2ControlPlane"
                    (and .Values.proxies.http_proxy (eq .Values.cluster.capi_providers.bootstrap_provider "cabpr")) | include "set-only-if" }}'
        - '{{ tuple "../../components/registry-mirror/KubeadmControlPlane"
                    (and .Values.dockerio_registry_mirror (eq .Values.cluster.capi_providers.bootstrap_provider "cabpk")) | include "set-only-if" }}'
        - '{{ tuple "../../components/registry-mirror/RKE2ControlPlane"
                    (and .Values.dockerio_registry_mirror (eq .Values.cluster.capi_providers.bootstrap_provider "cabpr")) | include "set-only-if" }}'
        - '{{ tuple "../../components/capo-root-volume"
                    (and .Values.cluster.capo.rootVolume (eq .Values.cluster.capi_providers.infra_provider "capo"))  | include "set-only-if" }}'

  calico:
    enabled: '{{ .Values.cluster.capi_providers.bootstrap_provider | eq "cabpk" | include "preserve-type" }}'
    depends_on:
      - name: cluster
    helm_repo_url: https://projectcalico.docs.tigera.io/charts
    helmrelease_spec:
      targetNamespace: tigera-operator
      install:
        createNamespace: true
      chart:
        spec:
          chart: tigera-operator
          version: v3.24.5

  cinder-csi:
    enabled: '{{ .Values.cluster.capi_providers.infra_provider | eq "capo" | include "preserve-type" }}'
    helm_repo_url: https://kubernetes.github.io/cloud-provider-openstack
    helmrelease_spec:
      chart:
        spec:
          chart: openstack-cinder-csi
          version: 2.3.0
      targetNamespace: cinder-csi
      install:
        createNamespace: true
      values:
        storageClass:
          enabled: false
          delete:
            isDefault: false
            allowVolumeExpansion: true
          retain:
            isDefault: false
            allowVolumeExpansion: true
          custom: |-
            ---
            apiVersion: storage.k8s.io/v1
            kind: StorageClass
            metadata:
              name: "{{ .Values.cluster.capo.storageClass.name }}"
            provisioner: cinder.csi.openstack.org
            volumeBindingMode: Immediate
            reclaimPolicy: Delete
            allowVolumeExpansion: true
            parameters:
              type: "{{ .Values.cluster.capo.storageClass.type }}"
    helm_secret_values:
      secret:
        enabled: "true"
        create: "true"
        name: cinder-csi-cloud-config
        data:
          cloud.conf: |-
            [Global]
            auth-url = "{{ .Values.cluster.capo.clouds_yaml.clouds.capo_cloud.auth.auth_url }}"
            tenant-name = "{{ .Values.cluster.capo.clouds_yaml.clouds.capo_cloud.auth.project_name }}"
            domain-name = "{{ .Values.cluster.capo.clouds_yaml.clouds.capo_cloud.auth.user_domain_name }}"
            username = "{{ .Values.cluster.capo.clouds_yaml.clouds.capo_cloud.auth.username }}"
            password = "{{ .Values.cluster.capo.clouds_yaml.clouds.capo_cloud.auth.password }}"
            region = "{{ .Values.cluster.capo.clouds_yaml.clouds.capo_cloud.auth.region }}"
            tls-insecure = "{{ not .Values.cluster.capo.clouds_yaml.clouds.capo_cloud.verify }}"
            [BlockStorage]
            ignore-volume-az = true

  imageswap-webhook:
    # today, only the rancher-webhook fix requires the use of imageswap-webhook
    enabled: '{{ .Values.units.rancher.enabled | include "preserve-type" }}'
    repo: sylva-core
    depends_on:
      - name: cert-manager
    kustomization_spec:
      path: ./kustomize-units/imageswap-webhook
      wait: true

  sylva-ca-certs:
    enabled: yes
    depends_on:
      - name: cert-manager
      - name: sylva-ca
    repo: sylva-core
    kustomization_spec:
      path: ./kustomize-units/sylva-ca-certs
      postBuild:
        substitute:
          RANCHER_DNS: '{{ .Values.cluster.rancher.external_hostname }}'
      wait: true

  rancher:
    enabled: yes
    depends_on:
      - name: cert-manager
      - name: k8s-gateway
      - name: imageswap-webhook   # cattle-system ns created by imageswap-webhook
      - name: sylva-ca-certs
      - '{{ tuple (dict "name" "ingress-nginx") (.Values.cluster.capi_providers.bootstrap_provider | eq "cabpk") | include "set-only-if" }}'
    helm_repo_url: https://releases.rancher.com/server-charts/stable
    helmrelease_spec:
      chart:
        spec:
          chart: rancher
          version: 2.6.9
      targetNamespace: cattle-system
      interval: 10m0s
      install:
        createNamespace: false    # cattle-system ns created by imageswap-webhook
        remediation:
          retries: 3
          remediateLastFailure: false
      upgrade:
        remediation:
          retries: 3
      values:
        privateCA: true
        bootstrapPassword: '{{ .Values.cluster.admin_password }}'
        useBundledSystemChart: true
        hostname: '{{ .Values.cluster.rancher.external_hostname }}'
        ingress:
          enabled: true
          ingressClassName: nginx
          tls:
            source: secret
            secretName: rancher-tls
        # restrictedAdmin: true
        # negative value will deploy 1 to abs(replicas) depending on available number of nodes
        replicas: -3
        features: embedded-cluster-api=false,provisioningv2=true
        debug: true
        proxy: '{{ get .Values.proxies "https_proxy" }}'
        noProxy: '{{ get .Values.proxies "no_proxy" }}'
        postDelete:
          namespaceList:
            - cattle-fleet-system
            - rancher-operator-system
      postRenderers:
        - kustomize:
            patchesStrategicMerge:
              - kind: Deployment
                apiVersion: apps/v1
                metadata:
                  name: rancher
                spec:
                  template:
                    spec:
                      volumes:
                        - name: tls-ca-volume
                          secret:
                            defaultMode: 256
                            secretName: rancher-tls
                            items:
                              - key: ca.crt
                                path: cacerts.pem
                      # this is to avoid that the too-short default liveness probe
                      # prevents the Rancher installation from finishing before the pod is killed:
                      containers:
                        - name: rancher
                          livenessProbe:
                            initialDelaySeconds: 120
                            periodSeconds: 30
                            failureThreshold: 20

  rancher-webhook-service:
    enabled: '{{ .Values.units.rancher.enabled | include "preserve-type" }}'
    repo: sylva-core
    depends_on:
      - name: rancher
    kustomization_spec:
      path: ./kustomize-units/rancher-webhook
      wait: true

  k8s-gateway:
    enabled: yes
    depends_on:
    - '{{ tuple (dict "name" "metallb-config") (eq .Values.cluster.capi_providers.infra_provider "capd") | include "set-only-if" }}'
    helm_repo_url: https://ori-edge.github.io/k8s_gateway/
    helmrelease_spec:
      chart:
        spec:
          chart: k8s-gateway
          version: 2.0.0
      interval: 1m0s
      values:
        domain: "sylva"
        replicaCount: 3
        service:
          loadBalancerIP: '{{ .Values.cluster.cluster_external_ip }}'
          annotations:
            metallb.universe.tf/allow-shared-ip: '{{ tuple (.Values.cluster.cluster_external_ip) (or (.Values.cluster.capi_providers.bootstrap_provider | eq "cabpr") (.Values.cluster.capi_providers.infra_provider | eq "capd")) | include "set-only-if" }}'

  metallb:
    enabled: '{{ .Values.cluster.capi_providers.infra_provider | eq "capd" | include "preserve-type" }}'
    repo: sylva-core
    depends_on:
    - name: cert-manager
    kustomization_spec:
      path: ./kustomize-units/metallb
      wait: true

  metallb-config:
    enabled: '{{ .Values.cluster.capi_providers.infra_provider | eq "capd" | include "preserve-type" }}'
    repo: sylva-core
    depends_on:
    - name: metallb
    kustomization_spec:
      path: ./kustomize-units/metallb-config
      wait: true
      postBuild:
        substitute:
          CLUSTER_EXTERNAL_IP: '{{ .Values.cluster.cluster_external_ip }}'

  capi-rancher-import:
    enabled: '{{ .Values.units.rancher.enabled | include "preserve-type" }}'
    repo: capi-rancher-import
    depends_on:
      - name: k8s-gateway
    helmrelease_spec:
      chart:
        spec:
          chart: charts/capi-rancher-import
          reconcileStrategy: Revision
          valuesFiles:
            - charts/capi-rancher-import/values.yaml
      interval: 1m0s
      values:
        conf:
          env:
            - name: URL_REMAP
              value: "https://{{ .Values.cluster.rancher.external_hostname }}/ https://rancher.cattle-system.svc.cluster.local/"
            - name: REQUESTS_SSL_VERIFY
              value: "no"
          cattle_agent_kustomize_source_ref:
            kind: GitRepository
            name: unit-capi-rancher-import
            namespace: default
          cattle_agent_kustomize_path: ./cattle-kustomize

  ingress-nginx:
    enabled: '{{ .Values.cluster.capi_providers.bootstrap_provider | eq "cabpk" | include "preserve-type" }}'
    depends_on:
    - '{{ tuple (dict "name" "metallb-config") (eq .Values.cluster.capi_providers.infra_provider "capd") | include "set-only-if" }}'
    helm_repo_url: https://kubernetes.github.io/ingress-nginx
    helmrelease_spec:
      chart:
        spec:
          chart: ingress-nginx
          version: 4.5.2
      interval: 1m0s
      values:
        controller:
          config:
            use-forwarded-headers: true
          kind: DaemonSet
          service:
            externalIPs:
            - '{{ .Values.cluster.cluster_external_ip }}'
            annotations:
              metallb.universe.tf/allow-shared-ip: '{{ tuple (.Values.cluster.cluster_external_ip) (eq .Values.cluster.capi_providers.infra_provider "capd") | include "set-only-if" }}'

  first-login-rancher:
    enabled: '{{ .Values.units.rancher.enabled | include "preserve-type" }}'
    repo: sylva-core
    depends_on:
      - name: rancher
    kustomization_spec:
      path: ./kustomize-units/kube-job
      wait: true
      force: true
      patches:
      - target:
          kind: Job
          name: kube-job
        patch: |
          - op: replace
            path: /metadata/name
            value: first-login-rancher-job
          - op: replace
            path: /spec/backoffLimit
            value: 10
      - target:
          kind: ConfigMap
          name: job-scripts
        patch: |
          - op: replace
            path: /data/kube-job.sh
            value: |
          {{ .Files.Get "scripts/first-login-rancher.sh" | indent 4 }}
      - target:
          kind: Job
          name: kube-job
        patch: |
          apiVersion: batch/v1
          kind: Job
          metadata:
            name: ignored
          spec:
            template:
              spec:
                containers:
                - name: run-script
                  env:
                  - name: RANCHER_EXTERNAL_URL
                    value: https://{{ .Values.cluster.rancher.external_hostname }}

  flux-webui:
    enabled: yes
    depends_on:
      - name: flux-system
      - '{{ tuple (dict "name" "ingress-nginx") (.Values.cluster.capi_providers.bootstrap_provider | eq "cabpk") | include "set-only-if" }}'
    repo: weave-gitops
    helmrelease_spec:
      chart:
        spec:
          chart: charts/gitops-server
      targetNamespace: flux-system
      install:
        createNamespace: false
      upgrade:
        force: true
      values:
        logLevel: debug
        envVars:
        - name: WEAVE_GITOPS_FEATURE_TENANCY
          value: "true"
        - name: WEAVE_GITOPS_FEATURE_CLUSTER
          value: "false"
        installCRDs: true
        rbac:
          additionalRules:
            - apiGroups: ["*"]
              resources: ["*"]
              verbs: [ "get", "list", "watch" ]
        ingress:
          enabled: true
          className: nginx
          hosts:
            - host: flux.sylva
              paths:
                - path: /   # setting this to another value like '/flux' does not work (URLs coming back from flux webui aren't rewritten by nginx)
                  pathType: Prefix
    helm_secret_values:
      adminUser:
        create: true
        username: admin
        passwordHash: '{{ htpasswd "" .Values.cluster.flux_webui.admin_password | trimPrefix ":" }}' # we don't want the "<user>:" part generated by htpasswd

  cluster-import:
    enabled: '{{ index .Values.units "workload-cluster" "enabled" | include "preserve-type" }}'
    repo: sylva-core
    depends_on:
      - name: first-login-rancher
      - name: workload-cluster
    kustomization_spec:
      path: ./kustomize-units/cluster-import
      postBuild:
        substitute:
          CLUSTER_FLAVOR: '{{ upper .Values.cluster.capi_providers.bootstrap_provider }} {{ upper .Values.cluster.capi_providers.infra_provider }}'
          CLUSTER_MONITORING: "enabled"
          CLUSTER_NAME: '{{ .Values.cluster.test_workload_cluster_name }}'
      wait: true

  monitoring:
    enabled: '{{ .Values.units.rancher.enabled | include "preserve-type" }}'
    repo: sylva-core
    depends_on:
      - name: rancher
    kustomization_spec:
      path: ./kustomize-units/monitoring
      wait: true
      postBuild:
        substitute:
          GIT_REPO: "https://gitlab.com/sylva-projects/sylva-elements/fleet-deployments/monitoring-stack.git"
          BRANCH: "main"

  # this is a Job attempt to check successful import of a CAPI workload clusters in Rancher, still incomplete
  # it would be used along with a definition of a 'workload-cluster' unit
  # and is conditioned by a boolean flag .Values.env_type_ci
  check-rancher-clusters:
    enabled: '{{ index .Values.units "workload-cluster" "enabled" | include "preserve-type" }}'
    repo: sylva-core
    depends_on:
      - name: cluster-import
    kustomization_spec:
      path: ./kustomize-units/kube-job
      wait: true
      force: true
      patches:
      - target:
          kind: Job
          name: kube-job
        patch: |
          - op: replace
            path: /metadata/name
            value: check-rancher-clusters-job
          - op: replace
            path: /spec/backoffLimit
            value: 10
      - target:
          kind: ConfigMap
          name: job-scripts
        patch: |
          - op: replace
            path: /data/kube-job.sh
            value: |
          {{ .Files.Get "scripts/check-rancher-clusters.sh" | indent 4 }}
      - target:
          kind: Job
          name: kube-job
        patch: |
          apiVersion: batch/v1
          kind: Job
          metadata:
            name: ignored
          spec:
            template:
              spec:
                containers:
                - name: run-script
                  env:
                  - name: CLUSTER_NAME
                    value: '{{ .Values.cluster.test_workload_cluster_name }}'

  workload-cluster:
    enabled: false
    repo: sylva-core
    depends_on:
      - name: capi
      - name: '{{ .Values.cluster.capi_providers.infra_provider }}'
      - name: '{{ .Values.cluster.capi_providers.bootstrap_provider }}'
      - name: rancher
      - name: first-login-rancher
    kustomization_spec:
      postBuild:
        substitute:
          CLUSTER_NAME: '{{ .Values.cluster.test_workload_cluster_name }}'
          MACHINE_IMAGE: '{{ .Values.cluster.image }}'
          K8SGATEWAY_LB_IP: '{{ .Values.cluster.cluster_external_ip }}'   # NOTE: same is used for k8s-gateway DNS svc in management cluster
        substituteFrom:
          - kind: ConfigMap
            name: proxy-env-vars
            optional: true
      wait: true
      components:
        - '{{ tuple "../../components/dns-client/kind/KubeadmControlPlane"
                    (and (eq .Values.cluster.capi_providers.infra_provider "capd") (eq .Values.cluster.capi_providers.bootstrap_provider "cabpk")) | include "set-only-if" }}'
        - '{{ tuple "../../components/dns-client/kind/RKE2ControlPlane"
                    (and (eq .Values.cluster.capi_providers.infra_provider "capd") (eq .Values.cluster.capi_providers.bootstrap_provider "cabpr")) | include "set-only-if" }}'
        - '{{ tuple "../../components/proxies/KubeadmControlPlane"
                    (and .Values.proxies.http_proxy (eq .Values.cluster.capi_providers.infra_provider "capd") (eq .Values.cluster.capi_providers.bootstrap_provider "cabpk")) | include "set-only-if" }}'
        - '{{ tuple "../../components/proxies/RKE2ControlPlane"
                    (and .Values.proxies.http_proxy (eq .Values.cluster.capi_providers.infra_provider "capd") (eq .Values.cluster.capi_providers.bootstrap_provider "cabpr")) | include "set-only-if" }}'
        - '{{ tuple "../../components/registry-mirror/KubeadmControlPlane"
                    (and .Values.dockerio_registry_mirror (eq .Values.cluster.capi_providers.bootstrap_provider "cabpk")) | include "set-only-if" }}'
        - '{{ tuple "../../components/registry-mirror/RKE2ControlPlane"
                    (and .Values.dockerio_registry_mirror (eq .Values.cluster.capi_providers.bootstrap_provider "cabpr")) | include "set-only-if" }}'

  workload-cluster-calico:
    enabled: '{{ index .Values.units "workload-cluster" "enabled" | include "preserve-type" }}'
    depends_on:
      - name: workload-cluster
    helm_repo_url: https://projectcalico.docs.tigera.io/charts
    helmrelease_spec:
      targetNamespace: tigera-operator
      install:
        createNamespace: true
      chart:
        spec:
          chart: tigera-operator
          version: v3.24.5
      kubeConfig:
        secretRef:
          name: '{{ .Values.cluster.test_workload_cluster_name }}-kubeconfig'

## stuff related to the 'cluster' unit

cluster:
  name: management-cluster
  # TODO: derive kubeconfig secret name from this ^

  # Admin password that will be configured by default on various units
  admin_password: '{{ randAlphaNum 64 }}'

  # image reference depends provider
  image: registry.gitlab.com/sylva-projects/sylva-elements/container-images/rke2-in-docker:v1-24-4-rke2r1

  # for now, the choice below needs to be made
  # consistently with the choice of a matching kustomization path
  # for the 'cluster' unit
  # e.g. you can use ./management-cluster-def/rke2-capd
  capi_providers:
    infra_provider: capd      # capd, capo, capm3 or capv
    bootstrap_provider: cabpk # cabpr (RKE2) or cabpk (kubeadm)

  # cis benchmark is only for rke2 so far, e.g. rke2-cis-1.23-profile-hardened
  cis_benchmark_scan_profile: '{{ eq .Values.cluster.capi_providers.bootstrap_provider "cabpr" | ternary "rke2-cis-1.23-profile-hardened" "no-scan-profile-defined-for-kubeadm-cluster" }}'

  capo:
    flavor_name: m1.large # Openstack flavor name
    ssh_key_name: # OpenStack VM SSH key
    network_id:  # OpenStack network to use as external network
    rootVolume: {} # Let this parameter empty if you don't intent to use root volume
    # otherwise, provide following values
    # diskSize: '20' # Size of the VMs root disk
    # volumeType: '__DEFAULT__' # Type of volume to be created
    clouds_yaml: # (this is a dict, not a YAML string)
      clouds:
        capo_cloud:
          auth:
            auth_url: # replace me
            user_domain_name: # replace me
            project_domain_name: # replace me
            project_name: # replace me
            username: # replace me
            password: # replace me
            region: # optional
          verify: # e.g. false
    #cacert: # cert used to validate CA of OpenStack APIs
    storageClass:
      name: foo  # name of the storageClass to be created
      type: xxx  # this is a cinder volume type e.g. 'ceph_sas' (must exist in OpenStack)

  cluster_external_ip: 55.55.55.55

  capd:
    docker_host: unix:///var/run/docker.sock

  flux_webui:
    admin_user: admin
    # admin password is to a random value by default, just to avoid setting a default that everyone would know
    # /!\ but the password set here will be generated by Helm and then hashed (see passwordHash in flux-webui
    # unit definition) so this random password will be unknown & unusable
    admin_password: '{{ .Values.cluster.admin_password }}'

  rancher:
    external_hostname: rancher.sylva

  vault:
    external_hostname: vault.sylva

  keycloak:
    external_hostname: keycloak.sylva

  metal3:
    ironic_ip: 66.66.66.66 # replace me (this IP is the IP at which Ironic is made accessible from BMCs or PXE network)


# add your proxy settings if required
proxies:
  https_proxy: ""
  http_proxy: ""
  no_proxy: ""

# add your local docker.io registry mirror to avoid rate limitting if required
dockerio_registry_mirror: ""

# boolean conditioning run of 'check-rancher-clusters' Job
env_type_ci: false
